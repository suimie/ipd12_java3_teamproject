
--login

CREATE TABLE login (
  id varchar(16) NOT NULL,
  employeeId INT IDENTITY(1,1)  NOT NULL,
  password varchar(50) NOT NULL,
  isAdmin varchar(50) NOT NULL,
  lastLoginTime timestamp NOT NULL,
  CHECK (isAdmin IN ('true','false')),
  CONSTRAINT login_id_pk PRIMARY KEY (id),
  CONSTRAINT fk_login_employee FOREIGN KEY (employeeId) REFERENCES employees (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;


CREATE INDEX fk_login_employee_idx
ON login (employeeId);

--Employees

CREATE TABLE employees (
  id INT IDENTITY(1,1)  NOT NULL,
  firstName varchar(100) NOT NULL,
  lastName varchar(100) NOT NULL,
  dob date NOT NULL,
  gender varchar(50) NOT NULL,
  email varchar(100) NOT NULL,
  contact varchar(20) NOT NULL,
  addr1 varchar(50) NOT NULL,
  addr2 varchar(50) DEFAULT NULL,
  addrNo int NOT NULL,
  postalCode varchar(6) NOT NULL,
  dept varchar(50) NOT NULL,
  hiredDate date NOT NULL,
  jobTitle varchar(100) NOT NULL,
  basicSalary decimal(10,2) NOT NULL,
  overtimeRate decimal(10,2) NOT NULL,
  photo VARBINARY(MAX),
  CHECK (gender IN ('Female','Male','NA')),
  CHECK (dept IN('Accounting','HR','IT','Sales')),
  CHECK (jobTitle IN('Accountant','Clerk','IT Developer','Manager','Salesperson')),
  CONSTRAINT employees_id_pk PRIMARY KEY (id)
  );

  --deduction rates

  CREATE TABLE deductionrates (
  id INT IDENTITY(1,1)  NOT NULL,
  year int NOT NULL UNIQUE,
  qppRate decimal(10,4) NOT NULL,
  qppThreshold decimal(10,2) NOT NULL,
  eiRate decimal(10,4) NOT NULL,
  eiThreshold decimal(10,2) NOT NULL,
  qpipRate decimal(10,4) NOT NULL,
  qpipThreshold decimal(10,2) NOT NULL,
  CONSTRAINT deductionrates_id_pk PRIMARY KEY (id)
  );

  --taxrates

  CREATE TABLE taxrates (
  id INT IDENTITY(1,1)  NOT NULL,
  year int NOT NULL,
  class varchar(10) NOT NULL,
  Min decimal(10,2) NOT NULL,
  Max decimal(10,2) NOT NULL,
  rate decimal(10,4) NOT NULL,
  type varchar(50) NOT NULL,
  CHECK (class IN ('A','B','C','D','E')),
  CHECK (type IN ('Federal','Provincial')),
  CONSTRAINT taxrates_id_pk PRIMARY KEY (id),
  CONSTRAINT unique_year_class_type UNIQUE (year,class,type)
);

--payslips

CREATE TABLE payslips (
  id INT IDENTITY(1,1)  NOT NULL,
  employeeId int NOT NULL,
  year int NOT NULL,
  periodFrom date NOT NULL,
  periodTo date DEFAULT NULL,
  fedTaxAmount decimal(10,2) NOT NULL DEFAULT '0.00',
  provTaxAmount decimal(10,2) NOT NULL DEFAULT '0.00',
  qppAmount decimal(10,2) NOT NULL DEFAULT '0.00',
  eiAmount decimal(10,2) NOT NULL DEFAULT '0.00',
  qpipAmount decimal(10,2) NOT NULL DEFAULT '0.00',
  overtimeHour decimal(10,2) NOT NULL DEFAULT '0.00',
  bonus decimal(10,2) NOT NULL DEFAULT '0.00',
  CONSTRAINT payslips_id_pk PRIMARY KEY (id),
  CONSTRAINT unique_emp_allow_from_to UNIQUE (periodTo, periodFrom, employeeId),
  CONSTRAINT fk_payslips_emloyees FOREIGN KEY (employeeId) REFERENCES employees (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE INDEX fk_payslips_employees_idx
ON payslips (employeeId);

 

 








