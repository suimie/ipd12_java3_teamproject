/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author suimi
 */
public class User {
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private int employeeId;
    private String id;
    private String password;
    private boolean isAdmin;
    private Date lastLogin;

    public User(int employeeId, String id, String password, boolean isAdmin) {
            setEmployeeId(employeeId);
            setId(id);
            setPassword(password);
            setIsAdmin(isAdmin);
    }

    public User(int employeeId, String id, String password, String isAdmin) {
            setEmployeeId(employeeId);
            setId(id);
            setPassword(password);
            setIsAdmin(isAdmin);
    }    
    
    public User(int employeeId, String id, String password, String isAdmin, java.sql.Date lastLogin) {
            setEmployeeId(employeeId);
            setId(id);
            setPassword(password);
            setIsAdmin(isAdmin);
            setLastLogin(lastLogin);
    }    
    
    /**
     * @return the employeeId
     */
    public int getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {        
        this.password = password;
    }

    /**
     * @return the isAdmin
     */
    public boolean isAdmin() {
        return isAdmin;
    }
    
    public String isAdmintStr() {
        return String.valueOf(isAdmin);
    }

    /**
     * @param isAdmin the isAdmin to set
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }    

    public void setIsAdmin(String isAdminStr) {
        this.isAdmin = Boolean.parseBoolean(isAdminStr);
    }
    
    /**
     * @return the lastLogin
     */
    public Date getLastLogin() {
        return lastLogin;
    }

    public String getLastLoginStr() {
        return dateFormat.format(lastLogin);
    }
    
    public java.sql.Date getLastLoginSQL() {
        return new java.sql.Date(lastLogin.getTime());
    }
    
    /**
     * @param lastLogin the lastLogin to set
     */
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    
    public void setLastLogin(String lastLoginStr) throws ParseException {
        this.lastLogin = dateFormat.parse(lastLoginStr);
    }
    
    public void setLastLogin(java.sql.Date lastLogin) {
        this.lastLogin = new Date(lastLogin.getTime());
    }
    
}
