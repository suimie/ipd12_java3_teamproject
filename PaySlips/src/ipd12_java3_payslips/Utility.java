/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

/**
 *
 * @author suimi
 */
public class Utility {
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final DecimalFormat currencyFormat = new DecimalFormat("###,###,###.##");
    public static String  userId;
    public static long employeeId;
    public static boolean isAdmin = true;
    
    public static String encrypt(String target, String key) {
        char cipher = Utility.getCipher(key);
        String newpwd = "";
        for(int i=target.length()-1; i>=0; i--){
                   char a = target.charAt(i);
                   a += cipher;
                   newpwd+=a;
        }
        
        return newpwd;
    }

    public static char getCipher(String key) {
        char cipher = key.charAt(3);
        cipher %= key.length();
        
        return cipher;
    }
    
    
    public static String getCurrencyFormattedAmount(BigDecimal amount) {
        return currencyFormat.format(amount);
    }
    
    public static BigDecimal getBigDecimalFromCurrencyFormattedString(String amount) throws ParseException {
        String newAmount = currencyFormat.parse(amount) + "";
        return new BigDecimal(newAmount);
    }
    
    public static String getDateString(Date date) {
        return dateFormat.format(date);
    }
    
    public static Date toDate(String dateStr) throws ParseException {
        return dateFormat.parse(dateStr);
    }
    
    public static java.sql.Date toSQLDate(String dateStr) throws ParseException {
        return new java.sql.Date(toDate(dateStr).getTime());
    }
    
    public static int getYear(Date date) {
         SimpleDateFormat format = new SimpleDateFormat("yyyy");
         String yearStr = format.format(date);
         int year = Integer.parseInt(yearStr);
         
         return year;
    }
    
    public static int getMonth(Date date) {
         SimpleDateFormat format = new SimpleDateFormat("MM");
         String monthStr = format.format(date);
         int month = Integer.parseInt(monthStr);
         
         return month;
    }

    public static int getDayOfMonth(Date date) {
         SimpleDateFormat format = new SimpleDateFormat("dd");
         String dateStr = format.format(date);
         int day = Integer.parseInt(dateStr);
         
         return day;
    }
    
    public static int getCurrentYear() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        Date today = new Date(System.currentTimeMillis());
        int year = Integer.parseInt(format.format(today));
        
        return year;
    }
    
    public static String getCurrentYearStr() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        Date today = new Date(System.currentTimeMillis());
        String year = format.format(today);
        
        return year;
    }
    
    public static ArrayList<PayDate> getPayDateList(Date hiredDate) throws ParseException{
        ArrayList<PayDate> list = new ArrayList();      
      
        int year = LocalDate.now().getYear();;
        int month = LocalDate.now().getMonthValue();;
        int day = LocalDate.now().getDayOfMonth();
        
        
        Calendar  cal = Calendar.getInstance();
        for(int i=0; i < 24; i++) {
            cal.set(year, month-1, 1);
            PayDate payDate1 = new PayDate(); 
            PayDate payDate2 = new PayDate();
       
            
            payDate1.from_year = year;
            payDate1.from_month = month;
            payDate1. from_day = 1;
            payDate1.to_year = year;
            payDate1.to_month = month;
            payDate1. to_day = 15;
                        
            String periodFromStr = payDate1.getFromDateString();
            Date periodFrom = dateFormat.parse(periodFromStr);
            
            if (hiredDate.after(periodFrom)){
                break;
            }
            
            if (i != 0 || day >= 16) {
                payDate2.from_year = year;
                payDate2.from_month = month;
                payDate2. from_day = 16;       
                payDate2.to_year = year;
                payDate2.to_month = month;
                payDate2. to_day = cal.getActualMaximum(Calendar.DATE);
            
                list.add(payDate2);
            }
            list.add(payDate1);
            
            if (month <= 1) {
                year--;
                month = 12;
            } else {
                month--;
            }
        }
        
        return list;
    }
    
    public static  void createPdf(String dest, PaySlip payslip, Employee employee) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        Rectangle titleRec = new Rectangle(490, 1000);
        document.open();
        PdfPTable tableEmployee = new PdfPTable(4);
        
        PdfPCell cell = new PdfPCell(new Phrase("Pay Slip"));
        cell.setColspan(4);
        cell.setFixedHeight(50);
        cell.setBorder(titleRec.NO_BORDER);
        tableEmployee.addCell(cell);        
        
        cell = new PdfPCell(new Phrase("Period"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase(payslip.getPeriodFromStr() + " ~ " + payslip.getPeriodToStr()));
        cell.setFixedHeight(30);
        cell.setColspan(3);
        tableEmployee.addCell(cell);                          
                
        cell = new PdfPCell(new Phrase("Employee ID "));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(String.format("%011d", payslip.getEmployeeId())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Name"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(employee.getFirstName() + " " + employee.getLastName()));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);


        cell = new PdfPCell(new Phrase("Date of Birth"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(employee.getDobString()));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Gender"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(employee.getGenderStr()));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        
        
        cell = new PdfPCell(new Phrase("Department"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(employee.getDeptStr()));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("Job Title"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(employee.getJobTitleStr()));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        
        cell = new PdfPCell(new Phrase("Basic Salary"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(Utility.getCurrencyFormattedAmount(employee.getBasicSalary().divide(new BigDecimal(24), RoundingMode.HALF_UP))));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        tableEmployee.addCell("");
        tableEmployee.addCell("");
        
        
        
        cell = new PdfPCell(new Phrase("Allowances"));
        cell.setFixedHeight(30);
        cell.setColspan(2);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Deductions"));
        cell.setFixedHeight(30);
        cell.setColspan(2);
        tableEmployee.addCell(cell);
        
        
        

        cell = new PdfPCell(new Phrase("Overtime Hours"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase(Utility.getCurrencyFormattedAmount(payslip.getOvertimeHour()) + " h"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Federal Incom Tax"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getFedTaxAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        

        cell = new PdfPCell(new Phrase("Overtime Rates"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getOvertimeRate())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Provincial Incom Tax"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getProvTaxAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        

        
        cell = new PdfPCell(new Phrase("Overtime Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getOvertimeAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("QPP Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getQppAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);



        cell = new PdfPCell(new Phrase("Bonus Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getBonus())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("EI Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getEiAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        

        cell = new PdfPCell(new Phrase("Total Allowance Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getTotalAllowance())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("QPIP Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getQpipAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        

        cell = new PdfPCell(new Phrase("Gross Pay Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getGrossPayAmount(employee.getBasicSalary()))));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Total Deduction Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getTotalDeductionAmount())));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
                

        cell = new PdfPCell(new Phrase("Net Pay Amount"));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);

        cell = new PdfPCell(new Phrase("$" + Utility.getCurrencyFormattedAmount(payslip.getGrossPayAmount(employee.getBasicSalary()))));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        cell = new PdfPCell(new Phrase(""));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
      
        cell = new PdfPCell(new Phrase(""));
        cell.setFixedHeight(30);
        tableEmployee.addCell(cell);
        
        
        
        document.add(tableEmployee);

        document.close();
    }    

}
