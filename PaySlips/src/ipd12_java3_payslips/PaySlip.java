/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Date;
import javax.swing.plaf.basic.BasicGraphicsUtils;

/**
 *
 * @author suimi
 */
public class PaySlip {
    private long employeeId;
    private Date periodFrom;
    private Date periodTo;    
    private BigDecimal overtimeHour;
    private BigDecimal overtimeRate;
    private BigDecimal bonus;
    private BigDecimal fedTaxAmount;
    private BigDecimal provTaxAmount;
    private BigDecimal qppAmount;
    private BigDecimal eiAmount;
    private BigDecimal qpipAmount;

    public PaySlip(long employeeId, Date periodFrom, Date periodTo, BigDecimal overtimeHour, 
            BigDecimal overtimeRate, BigDecimal bonus, BigDecimal fedTaxAmount, 
            BigDecimal provTaxAmount, BigDecimal qppAmount, BigDecimal eiAmount, 
            BigDecimal qpipAmount) {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFrom);
        setPeriodTo(periodTo);
        setOvertimeHour(overtimeHour);
        setOvertimeRate(overtimeRate);
        setBonus(bonus);
        setFedTaxAmount(fedTaxAmount);
        setProvTaxAmount(provTaxAmount);
        setQppAmount(qppAmount);
        setEiAmount(eiAmount);
        setQpipAmount(qpipAmount);
    }

    public PaySlip(long employeeId, java.sql.Date periodFrom, java.sql.Date periodTo, BigDecimal overtimeHour, 
            BigDecimal overtimeRate, BigDecimal bonus, BigDecimal fedTaxAmount, 
            BigDecimal provTaxAmount, BigDecimal qppAmount, BigDecimal eiAmount, 
            BigDecimal qpipAmount) {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFrom);
        setPeriodTo(periodTo);
        setOvertimeHour(overtimeHour);
        setOvertimeRate(overtimeRate);
        setBonus(bonus);
        setFedTaxAmount(fedTaxAmount);
        setProvTaxAmount(provTaxAmount);
        setQppAmount(qppAmount);
        setEiAmount(eiAmount);
        setQpipAmount(qpipAmount);
    }
    
    public PaySlip(long employeeId,String periodFromStr, String periodToStr, BigDecimal overtimeHour, 
            BigDecimal overtimeRate, BigDecimal bonus, BigDecimal fedTaxAmount, 
            BigDecimal provTaxAmount, BigDecimal qppAmount, BigDecimal eiAmount, 
            BigDecimal qpipAmount) throws ParseException {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFromStr);
        setPeriodTo(periodToStr);
        setOvertimeHour(overtimeHour);
        setOvertimeRate(overtimeRate);
        setBonus(bonus);
        setFedTaxAmount(fedTaxAmount);
        setProvTaxAmount(provTaxAmount);
        setQppAmount(qppAmount);
        setEiAmount(eiAmount);
        setQpipAmount(qpipAmount);
    }
    
    public PaySlip(long employeeId, Date periodFrom, Date periodTo, BigDecimal overtimeRate) {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFrom);
        setPeriodTo(periodTo);        
        setOvertimeRate(overtimeRate);
        
        setOvertimeHour(0);
        setBonus(0);
        setFedTaxAmount(0);
        setProvTaxAmount(0);
        setQppAmount(0);
        setEiAmount(0);
        setQpipAmount(0);
    }
    
    public PaySlip(long employeeId, java.sql.Date periodFrom, java.sql.Date periodTo, BigDecimal overtimeRate) {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFrom);
        setPeriodTo(periodTo);        
        setOvertimeRate(overtimeRate);
        
        setOvertimeHour(0);
        setBonus(0);
        setFedTaxAmount(0);
        setProvTaxAmount(0);
        setQppAmount(0);
        setEiAmount(0);
        setQpipAmount(0);
    }
    
    public PaySlip(long employeeId, String periodFromStr, String periodToStr, BigDecimal overtimeRate) throws ParseException {
        setEmployeeId(employeeId);
        setPeriodFrom(periodFromStr);
        setPeriodTo(periodToStr);    
        setOvertimeRate(overtimeRate);
        
        setOvertimeHour(0);
        setBonus(0);
        setFedTaxAmount(0);
        setProvTaxAmount(0);
        setQppAmount(0);
        setEiAmount(0);
        setQpipAmount(0);
    }
    /**
     * @return the employeeId
     */
    public long getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the periodFrom
     */
    public Date getPeriodFrom() {
        return periodFrom;
    }

    public java.sql.Date getPeriodFromSQL() {
        return new java.sql.Date(periodFrom.getTime());
    }
    
    public String getPeriodFromStr() {
        return Utility.getDateString(periodFrom);
    }
    
    /**
     * @param periodFrom the periodFrom to set
     */
    public void setPeriodFrom(Date periodFrom) {
        this.periodFrom = periodFrom;
    }
    
    public void setPeriodFrom(java.sql.Date periodFrom) {
        this.periodFrom = new Date(periodFrom.getTime());
    }

    public void setPeriodFrom(String periodFromStr) throws ParseException {
        this.periodFrom = Utility.toDate(periodFromStr);
    }
    
   
    /**
     * @return the periodTo
     */
    public Date getPeriodTo() {
        return periodTo;
    }

    public java.sql.Date getPeriodToSQL() {
        return new java.sql.Date(periodTo.getTime());
    }
    
    public String getPeriodToStr() {
        return Utility.getDateString(periodTo);
    }
    
    /**
     * @param periodTo the periodTo to set
     */
    public void setPeriodTo(Date periodTo) {
        this.periodTo = periodTo;
    }
    
    public void setPeriodTo(java.sql.Date periodTo) {
        this.periodTo = new Date(periodTo.getTime());
    }

    public void setPeriodTo(String periodToStr) throws ParseException {
        this.periodTo = Utility.toDate(periodToStr);
    }    
    
    public int getYear() {
        return Utility.getYear(periodFrom);
    }

    /**
     * @return the overtimeHour
     */
    public BigDecimal getOvertimeHour() {
        return overtimeHour;
    }

    /**
     * @param overtimeHour the overtimeHour to set
     */
    public void setOvertimeHour(BigDecimal overtimeHour) {
        this.overtimeHour = overtimeHour;
    }

    public void setOvertimeHour(int overtimeHour) {
        this.overtimeHour = new BigDecimal(overtimeHour);
    }
    /**
     * @return the overtimeRate
     */
    public BigDecimal getOvertimeRate() {
        return overtimeRate;
    }

    /**
     * @param overtimeRate the overtimeRate to set
     */
    public void setOvertimeRate(BigDecimal overtimeRate) {
        this.overtimeRate = overtimeRate;
    }

    public void setOvertimeRate(int overtimeRate) {
        this.overtimeRate = new BigDecimal(overtimeRate);
    }
    
    /**
     * @return the overtimeAmount
     */
    public BigDecimal getOvertimeAmount() {
        if (overtimeHour.equals(BigDecimal.ZERO) || overtimeRate.equals(BigDecimal.ZERO)){
            return BigDecimal.ZERO;
        }
        
        return overtimeHour.multiply(overtimeRate);
    }


    /**
     * @return the bonus
     */
    public BigDecimal getBonus() {
        return bonus;
    }


    /**
     * @param bonus the bonus to set
     */
    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = new BigDecimal(bonus);
    }
        
    /**
     * @return the totalAllowance
     */
    public BigDecimal getTotalAllowance() {        
        return bonus.add(getOvertimeAmount());
    }


    /**
     * @return the fedTaxAmount
     */
    public BigDecimal getFedTaxAmount() {
        return fedTaxAmount;
    }


    /**
     * @param fedTaxAmount the fedTaxAmount to set
     */
    public void setFedTaxAmount(BigDecimal fedTaxAmount) {
        this.fedTaxAmount = fedTaxAmount;
    }

    public void setFedTaxAmount(int fedTaxAmount) {
        this.fedTaxAmount = new BigDecimal(fedTaxAmount);
    }
    
    /**
     * @return the provTaxAmount
     */
    public BigDecimal getProvTaxAmount() {
        return provTaxAmount;
    }


    /**
     * @param provTaxAmount the provTaxAmount to set
     */
    public void setProvTaxAmount(BigDecimal provTaxAmount) {
        this.provTaxAmount = provTaxAmount;
    }

    public void setProvTaxAmount(int provTaxAmount) {
        this.provTaxAmount = new BigDecimal(provTaxAmount);
    }
    
    /**
     * @return the qppAmount
     */
    public BigDecimal getQppAmount() {
        return qppAmount;
    }


    /**
     * @param qppAmount the qppAmount to set
     */
    public void setQppAmount(BigDecimal qppAmount) {
        this.qppAmount = qppAmount;
    }

    public void setQppAmount(int qppAmount) {
        this.qppAmount = new BigDecimal(qppAmount);
    }    
    /**
     * @return the eiAmount
     */
    public BigDecimal getEiAmount() {
        return eiAmount;
    }


    /**
     * @param eiAmount the eiAmount to set
     */
    public void setEiAmount(BigDecimal eiAmount) {
        this.eiAmount = eiAmount;
    }

    public void setEiAmount(int eiAmount) {
        this.eiAmount = new BigDecimal(eiAmount);
    }    

    /**
     * @return the qpipAmount
     */
    public BigDecimal getQpipAmount() {
        return qpipAmount;
    }


    /**
     * @param qpipAmount the qpipAmount to set
     */
    public void setQpipAmount(BigDecimal qpipAmount) {
        this.qpipAmount = qpipAmount;
    }
    
    public void setQpipAmount(int qpipAmount) {
        this.qpipAmount = new BigDecimal(qpipAmount);
    }    
    
    public BigDecimal getTotalDeductionAmount() {
        return  ((eiAmount.add(qppAmount)).add(qpipAmount).add(getFedTaxAmount())).add(getProvTaxAmount());
    }

 
    public BigDecimal getGrossPayAmount(BigDecimal basicSalary) {
        if (basicSalary.equals(BigDecimal.ZERO)){
            return getTotalAllowance();
        }
        
        return getTotalAllowance().add(basicSalary.divide(new BigDecimal(24), RoundingMode.HALF_UP));
    }
    
    public BigDecimal getNetPayAmount(BigDecimal basicSalary) {
        return getGrossPayAmount(basicSalary).subtract(getTotalDeductionAmount());
    }
    
}
