/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.math.BigDecimal;

/**
 *
 * @author suimi
 */
public class TaxAmount {
    TaxRate.TaxType taxType;
    private BigDecimal amount;
    private BigDecimal totAmount;

    public TaxAmount(TaxRate.TaxType taxType, BigDecimal amount) {
        setTaxType(taxType);
        setAmount(amount);
    }
    
    public TaxAmount(String taxType, BigDecimal amount) {
        setTaxType(taxType);
        setAmount(amount);
    }
        
    public TaxRate.TaxType getTaxType() {
        return taxType;
    }

    public String getTaxTypeStr() {
        return taxType.toString();
    }
    /**
     * @param type the type to set
     */
    public void setTaxType(TaxRate.TaxType taxType) {
        this.taxType = taxType;
    }

    public void setTaxType(String typeStr) {
        this.taxType = TaxRate.TaxType.valueOf(typeStr);
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    /**
     * @return the totAmount
     */
    public BigDecimal getTotAmount() {
        return totAmount;
    }

    /**
     * @param amount the amount to set
     */
    public void setTotAmount(BigDecimal totAmount) {
        this.totAmount = totAmount;
    }    
}
