


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package ipd12_java3_payslips;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author 1796138(Suim Park)
 */
public class Database {

    /*
    private final static String HOSTNAME = "valdatabase.database.windows.net";
    private final static String DBNAME = "payroll";
    private final static String USERNAME = "valdatabase";
    private final static String PASSWORD = "Valiniipd12";
*/
    /*
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "epms";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";
    */
    private Connection conn;
    private static Database instance;
    
    class RecordNotFoundException extends SQLException {
        public RecordNotFoundException() { }
        public RecordNotFoundException(String msg) { super(msg); }
        public RecordNotFoundException(String msg, Throwable cause) { super(msg, cause); }
    }

    /*
    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);
    }
    */
     public Database() throws SQLException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            // exception chaining
            throw new SQLException("Driver not found", ex);
        }
        
             conn = DriverManager.getConnection(
                "jdbc:sqlserver://valdatabase.database.windows.net:1433;database=payroll;user=valdatabase@valdatabase;password=Valiniipd12;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");  
    }  

    public Connection getConnection() {
        return conn;
    }

    public static Database getInstance() throws SQLException {
        if (instance == null) {
            instance = new Database();
        } else if (instance.getConnection().isClosed()) {
            instance = new Database();
        }

        return instance;
    }

    public int isExist(String tableName, String where) throws SQLException {
        String sql = "SELECT COUNT(*) FROM " + tableName + where;
        int count = 0;

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                count = result.getInt(1);

                return count;
            } else {
                return 0;
            }
        }
    }

    public void addUserIntoLogin(int employeeId, String id, String pwd, boolean isAdmin) throws SQLException, Exception {
        String sql = "INSERT INTO login (id, employeeId, password, isAdmin) VALUES (?, ?, ?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, id);
            stmt.setInt(2, employeeId);
            stmt.setString(3, Utility.encrypt(pwd, id));
            stmt.setString(4, (isAdmin ? "true" : "false"));

            stmt.executeUpdate();
        }
    }

    public User getUserFromLogin(String id) throws SQLException {

        String sql = "SELECT * FROM login WHERE id='" + id + "'";

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);

            if (result.next()) {
                int employeeId = result.getInt("employeeId");
                String password = result.getString("password");
                String isAdmin = result.getString("isAdmin");
                java.sql.Timestamp lastLogin = result.getTimestamp("lastLoginTime");

                User user = new User(employeeId, id, password, isAdmin, new Date(lastLogin.getTime()));
                return user;
            } else {
                return null;
            }
        }
    }

    public void updateLastLoginTime(String id) throws SQLException {
        String sql = "UPDATE login SET lastLoginTime=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            stmt.setString(2, id);

            stmt.executeUpdate();
        }
    }

    public void updatePassword(String id, String password) throws SQLException, Exception {
        String sql = "UPDATE login SET password=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, Utility.encrypt(password, id));
            stmt.setString(2, id);

            stmt.executeUpdate();
        }
    }

    public void addTaxRate(TaxRate taxRate) throws SQLException {
        String sql = "INSERT INTO taxRates (year, class, Min, Max, rate, type) VALUES (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, taxRate.getYear());
            stmt.setString(2, taxRate.getTaxClassStr());
            stmt.setBigDecimal(3, taxRate.getMin());
            stmt.setBigDecimal(4, taxRate.getMax());
            stmt.setBigDecimal(5, taxRate.getRate());
            stmt.setString(6, taxRate.getTaxTypeStr());

            stmt.executeUpdate();
        }
    }

    public TaxRate getTaxRate(int year, String taxType, String taxClass) throws SQLException {

        String sql = "SELECT * FROM taxRates WHERE year="
                + year + " AND type='" + taxType + "' AND class='" + taxClass + "'";

        try (Statement stmt = conn.createStatement()) {

            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                BigDecimal min = result.getBigDecimal("min");
                BigDecimal max = result.getBigDecimal("max");
                BigDecimal rate = result.getBigDecimal("rate");

                TaxRate taxRate = new TaxRate(year, taxType, taxClass, min, max, rate);
                return taxRate;
            } else {
                return null;
            }
        }
    }


    public ArrayList<TaxRate> getTaxRateListForYearType(int year, String taxType) throws SQLException {
        ArrayList<TaxRate> taxList = new ArrayList<>();
        String sql = "SELECT * FROM taxRates WHERE year="
                + year + " AND type='" + taxType + "'";

        try (Statement stmt = conn.createStatement()) {

            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                BigDecimal min = result.getBigDecimal("min");
                BigDecimal max = result.getBigDecimal("max");
                BigDecimal rate = result.getBigDecimal("rate");
                String taxClass = result.getString("class");

                TaxRate taxRate = new TaxRate(year, taxType, taxClass, min, max, rate);
                taxList.add(taxRate);
            } 
            
            return taxList;
        }
    }

     public ArrayList<TaxRate> getTaxRateListForYear(int year) throws SQLException {
        ArrayList<TaxRate> taxList = new ArrayList<>();
        String sql = "SELECT * FROM taxRates WHERE year=" + year + " ORDER BY YEAR, TYPE, CLASS";

        try (Statement stmt = conn.createStatement()) {

            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                BigDecimal min = result.getBigDecimal("min");
                BigDecimal max = result.getBigDecimal("max");
                BigDecimal rate = result.getBigDecimal("rate");
                String taxClass = result.getString("class");
                String taxType = result.getString("type");

                TaxRate taxRate = new TaxRate(year, taxType, taxClass, min, max, rate);
                taxList.add(taxRate);
            } 
            
            return taxList;
        }
    }
     
    public void updateTaxRate(TaxRate taxRate) throws SQLException {
        String sql = "UPDATE taxRates SET Min=?, Max=?, rate=? WHERE year=? AND class=? AND type=?";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, taxRate.getMin());
            stmt.setBigDecimal(2, taxRate.getMax());
            stmt.setBigDecimal(3, taxRate.getRate());
            stmt.setInt(4, taxRate.getYear());
            stmt.setString(5, taxRate.getTaxClassStr());
            stmt.setString(6, taxRate.getTaxTypeStr());

            stmt.executeUpdate();
        }
    }

    public DeductionRates getOtherRate(int year) throws SQLException {

        String sql = "SELECT * FROM deductionrates WHERE year=" + year;

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            
            if (result.next()) {
                BigDecimal qppRate = result.getBigDecimal("qppRate");
                BigDecimal qppThreshold = result.getBigDecimal("qppThreshold");
                BigDecimal eiRate = result.getBigDecimal("eiRate");
                BigDecimal eiThreshold = result.getBigDecimal("eiThreshold");
                BigDecimal qpipRate = result.getBigDecimal("qpipRate");
                BigDecimal qpipThreshold = result.getBigDecimal("qpipThreshold");

                DeductionRates deduction = new DeductionRates(year, qppRate, qppThreshold, eiRate, eiThreshold, qpipRate, qpipThreshold);
                return deduction;
            } else {
                return null;
            }
        }
    }

    public void addOtherRate(DeductionRates deduction) throws SQLException {
        String sql = "INSERT INTO deductionrates "
                + "(year, qppRate, qppThreshold, eiRate, eiThreshold, qpipRate, qpipThreshold) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, deduction.getYear());
            stmt.setBigDecimal(2, deduction.getQppRate());
            stmt.setBigDecimal(3, deduction.getQppThreshold());
            stmt.setBigDecimal(4, deduction.getEiRate());
            stmt.setBigDecimal(5, deduction.getEiThreshold());
            stmt.setBigDecimal(6, deduction.getQpipRate());
            stmt.setBigDecimal(7, deduction.getQpipThreshold());

            stmt.executeUpdate();
        }
    }

    public void updateOtherRate(DeductionRates deduction) throws SQLException {
        String sql = "UPDATE deductionrates SET qppRate=?, qppThreshold=?, "
                + "eiRate=?, eiThreshold=?, qpipRate=?, qpipThreshold=? "
                + "WHERE year=?";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, deduction.getQppRate());
            stmt.setBigDecimal(2, deduction.getQppThreshold());
            stmt.setBigDecimal(3, deduction.getEiRate());
            stmt.setBigDecimal(4, deduction.getEiThreshold());
            stmt.setBigDecimal(5, deduction.getQpipRate());
            stmt.setBigDecimal(6, deduction.getQpipThreshold());
            stmt.setInt(7, deduction.getYear());

            stmt.executeUpdate();
        }
    }    

        public BigDecimal getSumOfFieldNameAmountBeforeDateForYear(String fieldName, int year, long employeeId, java.sql.Date periodFrom) throws SQLException{
        String sql = "SELECT SUM(" + fieldName + ") FROM payslips WHERE "
                + "year=? AND employeeId=? AND periodFrom<?";        
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, year);
            stmt.setInt(2, (int)employeeId);
            stmt.setDate(3, periodFrom);

            ResultSet rs = stmt.executeQuery();
            
            if (rs.next()) {
                BigDecimal amount = rs.getBigDecimal(1);
                
                if(amount == null) {
                    return new BigDecimal(0);
                }
                return amount;
            } else {
                return new BigDecimal(0);
            }
        }
    }
        
    
    public void addPaySlip(PaySlip paySlip) throws SQLException
    {
        String sql = "INSERT INTO payslips " 
                + "(employeeId, year, periodFrom, periodTo, fedTaxAmount, " 
                + "provTaxAmount, qppAmount, eiAmount, qpipAmount, overtimeHour, bonus) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, (int)paySlip.getEmployeeId());
            stmt.setInt(2, paySlip.getYear());
            stmt.setDate(3, paySlip.getPeriodFromSQL());
            stmt.setDate(4, paySlip.getPeriodToSQL());
            stmt.setBigDecimal(5, paySlip.getFedTaxAmount());
            stmt.setBigDecimal(6, paySlip.getProvTaxAmount());
            stmt.setBigDecimal(7, paySlip.getQppAmount());
            stmt.setBigDecimal(8, paySlip.getEiAmount());
            stmt.setBigDecimal(9, paySlip.getQpipAmount());
            stmt.setBigDecimal(10, paySlip.getOvertimeHour());
            stmt.setBigDecimal(11, paySlip.getBonus());
            
            stmt.executeUpdate();
        }
    }
    
    public void updatePaySlip(PaySlip paySlip) throws SQLException
    {
        String sql = "UPDATE  payslips SET " 
                + "fedTaxAmount=?, provTaxAmount=?, qppAmount=?, eiAmount=?, qpipAmount=?, overtimeHour=?, bonus=? "
                + " WHERE employeeId=? AND periodFrom=? AND periodTo=?";
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, paySlip.getFedTaxAmount());
            stmt.setBigDecimal(2, paySlip.getProvTaxAmount());
            stmt.setBigDecimal(3, paySlip.getQppAmount());
            stmt.setBigDecimal(4, paySlip.getEiAmount());
            stmt.setBigDecimal(5, paySlip.getQpipAmount());
            stmt.setBigDecimal(6, paySlip.getOvertimeHour());
            stmt.setBigDecimal(7, paySlip.getBonus());
            stmt.setInt(8, (int)paySlip.getEmployeeId());
            stmt.setDate(9, paySlip.getPeriodFromSQL());
            stmt.setDate(10, paySlip.getPeriodToSQL());
            
            stmt.executeUpdate();
        }
    }
    
    
    public PaySlip getPaySlip(long employeeId, BigDecimal overtimeRate, java.sql.Date periodFrom, java.sql.Date periodTo) throws SQLException {
        String sql = "SELECT * FROM payslips WHERE "
                + "employeeId=? AND periodFrom=? AND periodTo=?";        
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, (int)employeeId);
            stmt.setDate(2, periodFrom);
            stmt.setDate(3, periodTo);
            
            ResultSet rs = stmt.executeQuery();
            
            
            if (rs.next()) {
                BigDecimal fedTaxAmount = rs.getBigDecimal("fedTaxAmount");
                BigDecimal provTaxAmount = rs.getBigDecimal("provTaxAmount");
                BigDecimal qppAmount = rs.getBigDecimal("qppAmount");
                BigDecimal eiAmount = rs.getBigDecimal("eiAmount");
                BigDecimal qpipAmount = rs.getBigDecimal("qpipAmount");
                BigDecimal overtimeHour = rs.getBigDecimal("overtimeHour");
                BigDecimal bonus = rs.getBigDecimal("bonus");
                
                PaySlip payslip = new PaySlip(employeeId, periodFrom, periodTo, overtimeHour, overtimeRate, bonus, fedTaxAmount, provTaxAmount, qppAmount, eiAmount, qpipAmount);

                return payslip;
            } else {
                return null;
            }
        }
    }
    
    public int isExistSlipForEmployeePeriod(long employeeId, java.sql.Date periodFrom, java.sql.Date periodTo) throws SQLException{
        String sql = "SELECT COUNT(*) FROM payslips WHERE "
                + "employeeId=? AND periodFrom=? AND periodTo=?";  

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setInt(1, (int)employeeId);
                stmt.setDate(2, periodFrom);
                stmt.setDate(3, periodTo);

                ResultSet rs = stmt.executeQuery();


                if (rs.next()) {
                    int count = rs.getInt(1);

                    return count;
                } else {
                    return 0;
                }     
        }
    }
    
    public void addEmployee(Employee employee) throws SQLException, Exception {
        String sql = "INSERT INTO employees "
                + "(lastName, firstName, dob, gender, email, contact, addr1, addr2, addrNo, postalCode, dept, hiredDate,\n"
                + " jobTitle, basicSalary, overtimeRate) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?,? )";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, employee.getLastName());
            stmt.setString(2, employee.getFirstName());
            stmt.setDate(3, employee.getDobSql());
            stmt.setString(4, employee.getGenderStr());
            stmt.setString(5, employee.getEmail());
            stmt.setString(6, employee.getContact());
            stmt.setString(7, employee.getAddr1());
            stmt.setString(8, employee.getAddr2());
            stmt.setInt(9, employee.getAddrNo());
            stmt.setString(10, employee.getPostalCode());
            stmt.setString(11, employee.getDeptStr());
            stmt.setDate(12, employee.getHiredDateSql());
            stmt.setString(13, employee.getJobTitleStr());
            stmt.setBigDecimal(14, employee.getBasicSalary());
            stmt.setBigDecimal(15, employee.getOvertimeRate());

            stmt.executeUpdate();

        }
    }
     ArrayList<Employee> list = new ArrayList<>();
     public Employee getEmployeeById(long id) throws SQLException {
        
        String sql = "SELECT * FROM employees WHERE id=" + id;
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                String lastName = result.getString("lastName");
                String firstName = result.getString("firstName");
                java.sql.Date dobSql = result.getDate("dob");
                //String genderStr = result.getString("gender");
                String genderStr = result.getString("Gender");
                String email = result.getString("email");
                String contact = result.getString("contact");
                String addr1 = result.getString("addr1");
                String addr2 = result.getString("addr2");
                int addrNo = result.getInt("addrNo");
                String postalCode = result.getString("postalCode");
                String deptStr = result.getString("dept");
                java.sql.Date hiredDateSql = result.getDate("hiredDate");
                String jobTitleStr = result.getString("jobTitle");
                BigDecimal basicSalary = result.getBigDecimal("basicSalary");
                BigDecimal overtimeRate = result.getBigDecimal("overtimeRate");
                
                Employee employee = new Employee(id, lastName, firstName, dobSql, genderStr, email, contact, addr1, addr2, addrNo, postalCode, deptStr, hiredDateSql,
                    jobTitleStr, basicSalary, overtimeRate);
    
                return employee;
            } else {
                throw new RecordNotFoundException("Not found id=" + id);
                // return null;
            }
        }
    }
     
     public ArrayList<Employee> getAllEmployees() throws SQLException {
        String sql = "SELECT * FROM employees";
        ArrayList<Employee> list = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                long id = result.getLong("id");
                String lastName = result.getString("lastName");
                String firstName = result.getString("firstName");
                java.sql.Date dobSql = result.getDate("dob");
                //String genderStr = result.getString("gender");
                String genderStr = result.getString("Gender");
                String email = result.getString("email");
                String contact = result.getString("contact");
                String addr1 = result.getString("addr1");
                String addr2 = result.getString("addr2");
                int addrNo = result.getInt("addrNo");
                String postalCode = result.getString("postalCode");
                String deptStr = result.getString("dept");
                java.sql.Date hiredDateSql = result.getDate("hiredDate");
                String jobTitleStr = result.getString("jobTitle");
                BigDecimal basicSalary = result.getBigDecimal("basicSalary");
                BigDecimal overtimeRate = result.getBigDecimal("overtimeRate");
                Employee employee = new Employee(id, lastName, firstName, dobSql, genderStr, email, contact, addr1, addr2, addrNo, postalCode, deptStr, hiredDateSql,
                    jobTitleStr, basicSalary, overtimeRate);
                list.add(employee);
            }
        }
        return list;
    }
     
     public void updateEmployee(Employee employee) throws SQLException, Exception {
        String sql = "UPDATE employees SET lastName=?, firstName=?, dob=?, gender=?, email=?, contact=?, addr1=?, addr2=?,"
                + "addrNo=?, postalCode=?, dept=?, hiredDate=?, jobTitle=?, basicSalary=?,overtimeRate=? WHERE id=?";
                
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, employee.getLastName());
            stmt.setString(2, employee.getFirstName());
            stmt.setDate(3, employee.getDobSql());
            stmt.setString(4, employee.getGenderStr());
            stmt.setString(5, employee.getEmail());
            stmt.setString(6, employee.getContact());
            stmt.setString(7, employee.getAddr1());
            stmt.setString(8, employee.getAddr2());
            stmt.setInt(9, employee.getAddrNo());
            stmt.setString(10, employee.getPostalCode());
            stmt.setString(11, employee.getDeptStr());
            stmt.setDate(12, employee.getHiredDateSql());
            stmt.setString(13, employee.getJobTitleStr());
            stmt.setBigDecimal(14, employee.getBasicSalary());
            stmt.setBigDecimal(15, employee.getOvertimeRate());
            stmt.setLong(16, employee.getId());
            stmt.executeUpdate();

        }
    }
     
     public void deleteEmployeeById(Long id) throws SQLException {
        String sql = "DELETE FROM employees WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);            
            stmt.executeUpdate();
        }
    }
}
