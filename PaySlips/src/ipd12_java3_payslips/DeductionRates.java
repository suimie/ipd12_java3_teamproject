/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.math.BigDecimal;

/**
 *
 * @author suimi
 */
public class DeductionRates {
    private int year;
    private BigDecimal qppRate;
    private BigDecimal qppThreshold;
    private BigDecimal eiRate;
    private BigDecimal eiThreshold;
    private BigDecimal qpipRate;
    private BigDecimal qpipThreshold;

    public DeductionRates(int year, BigDecimal qppRate, BigDecimal qppThreshold, BigDecimal eiRate, BigDecimal eiThreshold, BigDecimal qpipRate, BigDecimal qpipThreshold) {
        setYear(year);
        setQppRate(qppRate);
        setQppThreshold(qppThreshold);
        setEiRate(eiRate);
        setEiThreshold(eiThreshold);
        setQpipRate(qpipRate);
        setQpipThreshold(qpipThreshold);
    }

    
    
    
    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the qppRate
     */
    public BigDecimal getQppRate() {
        return qppRate;
    }

    /**
     * @param qppRate the qppRate to set
     */
    public void setQppRate(BigDecimal qppRate) {
        this.qppRate = qppRate;
    }

    /**
     * @return the qppThreshold
     */
    public BigDecimal getQppThreshold() {
        return qppThreshold;
    }

    /**
     * @param qppThreshold the qppThreshold to set
     */
    public void setQppThreshold(BigDecimal qppThreshold) {
        this.qppThreshold = qppThreshold;
    }

    /**
     * @return the eiRate
     */
    public BigDecimal getEiRate() {
        return eiRate;
    }

    /**
     * @param eiRate the eiRate to set
     */
    public void setEiRate(BigDecimal eiRate) {
        this.eiRate = eiRate;
    }

    /**
     * @return the eiThreshold
     */
    public BigDecimal getEiThreshold() {
        return eiThreshold;
    }

    /**
     * @param eiThreshold the eiThreshold to set
     */
    public void setEiThreshold(BigDecimal eiThreshold) {
        this.eiThreshold = eiThreshold;
    }

    /**
     * @return the qpipRate
     */
    public BigDecimal getQpipRate() {
        return qpipRate;
    }

    /**
     * @param qpipRate the qpipRate to set
     */
    public void setQpipRate(BigDecimal qpipRate) {
        this.qpipRate = qpipRate;
    }

    /**
     * @return the qpipThreshold
     */
    public BigDecimal getQpipThreshold() {
        return qpipThreshold;
    }

    /**
     * @param qpipThreshold the qpipThreshold to set
     */
    public void setQpipThreshold(BigDecimal qpipThreshold) {
        this.qpipThreshold = qpipThreshold;
    }
    
    
}
