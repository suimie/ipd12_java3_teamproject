package ipd12_java3_payslips;

import javax.swing.JOptionPane;


public class EPMSMainWindow extends javax.swing.JFrame {


    public EPMSMainWindow() {
        initComponents();
        
        if (!Utility.isAdmin){
            miMgAllowDeduct.setEnabled(false);
            miMgEmployee.setEnabled(false);
            miTaxDeductionRates.setEnabled(false);
            btManageEmployee.setEnabled(false);
            btAllowDeduct.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblStatus = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btManageEmployee = new javax.swing.JButton();
        btSearchPrint = new javax.swing.JButton();
        btAllowDeduct = new javax.swing.JButton();
        jLabel49 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miLogout = new javax.swing.JMenuItem();
        miManage = new javax.swing.JMenu();
        miMgEmployee = new javax.swing.JMenuItem();
        miMgAllowDeduct = new javax.swing.JMenuItem();
        miTaxDeductionRates = new javax.swing.JCheckBoxMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miChangePwd = new javax.swing.JMenuItem();
        miSearchPrintSlip = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Employee Payroll Management System");
        setResizable(false);

        lblStatus.setText("Admin last login 2018-02-27 16:33:30");
        lblStatus.setMaximumSize(new java.awt.Dimension(34, 24));
        lblStatus.setMinimumSize(new java.awt.Dimension(34, 24));
        lblStatus.setPreferredSize(new java.awt.Dimension(34, 24));
        getContentPane().add(lblStatus, java.awt.BorderLayout.PAGE_END);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        btManageEmployee.setBackground(new java.awt.Color(255, 255, 255));
        btManageEmployee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add_Employee.png"))); // NOI18N
        btManageEmployee.setText("Manage Employee         ");
        btManageEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btManageEmployeeActionPerformed(evt);
            }
        });
        jPanel1.add(btManageEmployee);
        btManageEmployee.setBounds(620, 350, 200, 60);

        btSearchPrint.setBackground(new java.awt.Color(255, 255, 255));
        btSearchPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Search.png"))); // NOI18N
        btSearchPrint.setText("Search / Print Payment");
        btSearchPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSearchPrintActionPerformed(evt);
            }
        });
        jPanel1.add(btSearchPrint);
        btSearchPrint.setBounds(620, 492, 200, 59);

        btAllowDeduct.setBackground(new java.awt.Color(255, 255, 255));
        btAllowDeduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/allowance.png"))); // NOI18N
        btAllowDeduct.setText("Allowance/Deduction    ");
        btAllowDeduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAllowDeductActionPerformed(evt);
            }
        });
        jPanel1.add(btAllowDeduct);
        btAllowDeduct.setBounds(620, 420, 199, 59);

        jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/payroll2.jpg"))); // NOI18N
        jPanel1.add(jLabel49);
        jLabel49.setBounds(0, 0, 870, 590);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 871, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 871, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 590, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        miFile.setText("File");

        miLogout.setText("Logout");
        miLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miLogoutActionPerformed(evt);
            }
        });
        miFile.add(miLogout);

        jMenuBar1.add(miFile);

        miManage.setText("Manage");

        miMgEmployee.setText("Employee");
        miMgEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miMgEmployeeActionPerformed(evt);
            }
        });
        miManage.add(miMgEmployee);

        miMgAllowDeduct.setText("Allowances/Deductions");
        miMgAllowDeduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miMgAllowDeductActionPerformed(evt);
            }
        });
        miManage.add(miMgAllowDeduct);

        miTaxDeductionRates.setText("Tax / Deduction Rates");
        miTaxDeductionRates.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miTaxDeductionRatesActionPerformed(evt);
            }
        });
        miManage.add(miTaxDeductionRates);
        miManage.add(jSeparator1);

        miChangePwd.setText("Change Password");
        miChangePwd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miChangePwdActionPerformed(evt);
            }
        });
        miManage.add(miChangePwd);

        jMenuBar1.add(miManage);

        miSearchPrintSlip.setText("Search PrintSlip");
        miSearchPrintSlip.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                miSearchPrintSlipMouseClicked(evt);
            }
        });
        miSearchPrintSlip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSearchPrintSlipActionPerformed(evt);
            }
        });
        jMenuBar1.add(miSearchPrintSlip);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void btManageEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btManageEmployeeActionPerformed
        dlgEmploeeManage = new EmployeeManageDialog(this, true);
        dlgEmploeeManage.setVisible(true);
    }//GEN-LAST:event_btManageEmployeeActionPerformed

    private void btAllowDeductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAllowDeductActionPerformed
        dlgAllowanceDeduction = new AllowanceDeductionDialog(this, true);
        dlgAllowanceDeduction.pack();
        dlgAllowanceDeduction.setVisible(true);
    }//GEN-LAST:event_btAllowDeductActionPerformed

    private void btSearchPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSearchPrintActionPerformed
        dlgSearchPrintSlip = new SearchPrintSlipDialog(this, true);
        dlgSearchPrintSlip.pack();
        dlgSearchPrintSlip.setVisible(true);
    }//GEN-LAST:event_btSearchPrintActionPerformed

    private void miLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miLogoutActionPerformed
       Object[] options = {"Logout", "Cancel"};
        int decision = JOptionPane.showOptionDialog(this,
                "Are you sure logout?",
                "Confirm Logout",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null, 
                options, 
                options[1]); 

        if (decision == JOptionPane.YES_OPTION) {   
            this.setVisible(false);
            loginWindow.setVisible(true);
        }
    }//GEN-LAST:event_miLogoutActionPerformed

    private void miChangePwdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miChangePwdActionPerformed
        dlgChangePassword = new ChangePasswordDialog(this, true);
        dlgChangePassword.pack();
        dlgChangePassword.setVisible(true);
    }//GEN-LAST:event_miChangePwdActionPerformed

    private void miMgEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miMgEmployeeActionPerformed
        dlgEmploeeManage = new EmployeeManageDialog(this, true);
        dlgEmploeeManage.pack();
        dlgEmploeeManage.setVisible(true);
    }//GEN-LAST:event_miMgEmployeeActionPerformed

    private void miMgAllowDeductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miMgAllowDeductActionPerformed
        dlgAllowanceDeduction = new AllowanceDeductionDialog(this, true);
        dlgAllowanceDeduction.pack();
        dlgAllowanceDeduction.setVisible(true);
    }//GEN-LAST:event_miMgAllowDeductActionPerformed

    private void miTaxDeductionRatesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miTaxDeductionRatesActionPerformed
        dlgRateManage = new RatemanageDialog(this, true);
        dlgRateManage.pack();
        dlgRateManage.setVisible(true);
    }//GEN-LAST:event_miTaxDeductionRatesActionPerformed

    private void miSearchPrintSlipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSearchPrintSlipActionPerformed
        dlgSearchPrintSlip = new SearchPrintSlipDialog(this, true);
        dlgSearchPrintSlip.pack();
        dlgSearchPrintSlip.setVisible(true);
    }//GEN-LAST:event_miSearchPrintSlipActionPerformed

    private void miSearchPrintSlipMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_miSearchPrintSlipMouseClicked
        dlgSearchPrintSlip = new SearchPrintSlipDialog(this, true);
        dlgSearchPrintSlip.pack();
        dlgSearchPrintSlip.setVisible(true);
    }//GEN-LAST:event_miSearchPrintSlipMouseClicked

    public void setStatusMessage(String msg) {
            lblStatus.setText(msg);
    }
    
    public void setLoginWindow(LoginWindow lw){
        this.loginWindow = lw;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EPMSMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EPMSMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EPMSMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EPMSMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EPMSMainWindow().setVisible(true);
            }
        });
    }


    private LoginWindow loginWindow;
    private EmployeeManageDialog  dlgEmploeeManage;
    private AllowanceDeductionDialog dlgAllowanceDeduction;
    private SearchPrintSlipDialog dlgSearchPrintSlip;
    private ChangePasswordDialog dlgChangePassword;
    private RatemanageDialog dlgRateManage;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAllowDeduct;
    private javax.swing.JButton btManageEmployee;
    private javax.swing.JButton btSearchPrint;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JMenuItem miChangePwd;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miLogout;
    private javax.swing.JMenu miManage;
    private javax.swing.JMenuItem miMgAllowDeduct;
    private javax.swing.JMenuItem miMgEmployee;
    private javax.swing.JMenu miSearchPrintSlip;
    private javax.swing.JCheckBoxMenuItem miTaxDeductionRates;
    // End of variables declaration//GEN-END:variables
}
