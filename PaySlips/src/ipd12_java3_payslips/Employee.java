/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 1796138
 */
public class Employee {

    public static enum Gender {
        Female, Male, NA
    };

    public static enum Department {
        HR, Sales, IT, Accounting
    };

    public static enum JobTitle {
        Manager, Clerk, Developer, Salesperson, Accountant
    };
    private long id;
    private String lastName;
    private String firstName;
    private Date dob;
    private Gender gender;
    private String email;
    private String contact;
    private String addr1;
    private String addr2;
    private int addrNo;
    private String postalCode;
    private Department dept;
    private Date hiredDate;
    private JobTitle jobTitle;
    private BigDecimal basicSalary;
    private BigDecimal overtimeRate;

    public Employee(long id, String lastName, String firstName, Date dob, Gender gender, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, Department dept, Date hiredDate, JobTitle jobTitle, BigDecimal basicSalary, BigDecimal overtimeRate) {
        setId(id);
        setLastName(lastName);
        setFirstName(firstName);
        setDob(dob);
        setGender(gender);
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(dept);
        setHiredDate(hiredDate);
        setJobTitle(jobTitle);
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
//java.sql.Date

    public Employee(long id, String lastName, String firstName, java.sql.Date dobSql, Gender gender, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, Department dept, java.sql.Date hiredDateSql, JobTitle jobTitle, BigDecimal basicSalary, BigDecimal overtimeRate) {
        setId(id);
        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobSql);
        setGender(gender);
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(dept);
        setHiredDate(hiredDateSql);
        setJobTitle(jobTitle);
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
//String Date

    public Employee(long id, String lastName, String firstName, String dobStr, Gender gender, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, Department dept, String hiredDateStr, JobTitle jobTitle, BigDecimal basicSalary, BigDecimal overtimeRate) {
        setId(id);
        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobStr);
        setGender(gender);
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(dept);
        setHiredDate(hiredDateStr);
        setJobTitle(jobTitle);
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
//no id, string date

    public Employee(String lastName, String firstName, String dobStr, Gender gender, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, Department dept, String hiredDateStr, JobTitle jobTitle, BigDecimal basicSalary, BigDecimal overtimeRate) {

        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobStr);
        setGender(gender);
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(dept);
        setHiredDate(hiredDateStr);
        setJobTitle(jobTitle);
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
    public Employee(String lastName, String firstName, Date dobSql, Gender gender, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, Department dept, Date hiredDateSql, JobTitle jobTitle, BigDecimal basicSalary, BigDecimal overtimeRate) {

        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobSql);
        setGender(gender);
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(dept);
        setHiredDate(hiredDateSql);
        setJobTitle(jobTitle);
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
public Employee(String lastName, String firstName, Date dobSql, String genderStr, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, String deptStr, Date hiredDateSql, String jobTitleStr, BigDecimal basicSalary, BigDecimal overtimeRate) {

        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobSql);
        setGender(genderStr);  
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(deptStr);       
        setHiredDate(hiredDateSql);
        setJobTitle(jobTitleStr);   
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }

public Employee(long id, String lastName, String firstName, Date dobSql, String genderStr, String email, String contact, String addr1, String addr2, int addrNo, String postalCode, String deptStr, Date hiredDateSql, String jobTitleStr, BigDecimal basicSalary, BigDecimal overtimeRate) {

        setId(id);
        setLastName(lastName);
        setFirstName(firstName);
        setDob(dobSql);
        setGender(genderStr);  
        setEmail(email);
        setContact(contact);
        setAddr1(addr1);
        setAddr2(addr2);
        setAddrNo(addrNo);
        setPostalCode(postalCode);
        setDept(deptStr);       
        setHiredDate(hiredDateSql);
        setJobTitle(jobTitleStr);   
        setBasicSalary(basicSalary);
        setOvertimeRate(overtimeRate);
    }
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the dob
     */
    public Date getDob() {
        return dob;
    }

    public java.sql.Date getDobSql() {
        return new java.sql.Date(dob.getTime());
    }
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String getDobString() {
        return dateFormat.format(dob);
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    private void setDob(String dobStr) {
        try {
            dob = dateFormat.parse(dobStr);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Date of Birth invalid, format yyyy-MM-dd ");
        }
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    public String getGenderStr() {
        return gender.toString();
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setGender(String genderStr) {
        this.gender = Gender.valueOf(genderStr);
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
         if (!email.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")) {
                throw new IllegalArgumentException("Please, enter a valid email address");

            }
        this.email = email;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the addr1
     */
    public String getAddr1() {
        return addr1;
    }

    /**
     * @param addr1 the addr1 to set
     */
    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    /**
     * @return the addr2
     */
    public String getAddr2() {
        return addr2;
    }

    /**
     * @param addr2 the addr2 to set
     */
    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    /**
     * @return the addrNo
     */
    public int getAddrNo() {
        return addrNo;
    }

    /**
     * @param addrNo the addrNo to set
     */
    public void setAddrNo(int addrNo) {
        this.addrNo = addrNo;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        if (!postalCode.matches("^(?!.*[DFIOQU])[A-VXY][0-9][A-Z]●?[0-9][A-Z][0-9]$")) {
                throw new IllegalArgumentException("postal code is not valid");

            }
        this.postalCode = postalCode;
    }

    /**
     * @return the dept
     */
    public Department getDept() {
        return dept;
    }

    public String getDeptStr() {
        return dept.toString();
    }

    /**
     * @param dept the dept to set
     */
    public void setDept(Department dept) {
        this.dept = dept;
    }

    public void setDept(String deptStr) {
        this.dept = Department.valueOf(deptStr);
    }

    /**
     * @return the hiredDate
     */
    public Date getHiredDate() {
        return hiredDate;
    }

    public java.sql.Date getHiredDateSql() {
        return new java.sql.Date(hiredDate.getTime());
    }

    private SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

    public String getHiredDateStr() {
        return dateFormat1.format(hiredDate);
    }

    /**
     * @param hiredDate the hiredDate to set
     */
    public void setHiredDate(Date hiredDate) {
        this.hiredDate = hiredDate;
    }

    private void setHiredDate(String hiredDateStr) {
        try {
            hiredDate = dateFormat.parse(hiredDateStr);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Due date invalid");
        }
    }

    /**
     * @return the jobTitle
     */
    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public String getJobTitleStr() {
        return jobTitle.toString();
    }
    
  

    /**
     * @param jobTitle the jobTitle to set
     */
    public void setJobTitle(JobTitle jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setJobTitle(String jobTitleStr) {
        this.jobTitle = JobTitle.valueOf(jobTitleStr);
    }

    /**
     * @return the basicSalary
     */
    public BigDecimal getBasicSalary() {
        return basicSalary;
    }

    /**
     * @param basicSalary the basicSalary to set
     */
    public void setBasicSalary(BigDecimal basicSalary) {
        this.basicSalary = basicSalary;
    }

    /**
     * @return the overtimeRate
     */
    public BigDecimal getOvertimeRate() {
        return overtimeRate;
    }

    /**
     * @param overtimeRate the overtimeRate to set
     */
    public void setOvertimeRate(BigDecimal overtimeRate) {
        this.overtimeRate = overtimeRate;
    }
    
     public String toString() {
   
        return String.format("%d: %s, %s, %s, %s, %s", id, lastName, firstName,dept,jobTitle, hiredDate );
    }
}
