/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

/**
 *
 * @author suimi
 */
public class PayDate {
    int from_year;
    int from_month;
    int from_day;    
    int to_year;
    int to_month;
    int to_day;    
    
    public String getFromDateString(){
        return String.format("%d-%02d-%02d", from_year, from_month, from_day);
    }
    
        public String getToDateString(){
        return String.format("%d-%02d-%02d", to_year, to_month, to_day);
    }
        
}
