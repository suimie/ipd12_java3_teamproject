/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12_java3_payslips;

import java.math.BigDecimal;

/**
 *
 * @author suimi
 */
public class TaxRate {
        public enum TaxType {Federal, Provincial };
        public enum TaxClass {A, B, C, D, E };
        
        private int year;
        private TaxType taxType;
        private TaxClass taxClass;
        private BigDecimal min;
        private BigDecimal max;
        private BigDecimal rate;

    public TaxRate(int year, TaxType taxType, TaxClass taxClass, BigDecimal min, BigDecimal max, BigDecimal rate) {
        setYear(year);
        setTaxType(taxType);
        setTaxClass(taxClass);
        setMin(min);
        setMax(max);
        setRate(rate);
    }

    public TaxRate(int year, String taxTypeStr, String taxClassStr, BigDecimal min, BigDecimal max, BigDecimal rate) {
        setYear(year);
        setTaxType(taxTypeStr);
        setTaxClass(taxClassStr);
        setMin(min);
        setMax(max);
        setRate(rate);
    }        
        
    
    public TaxRate(String yearStr, String typeStr, String taxClassStr) {
        setYear(yearStr);
        setTaxType(typeStr);
        setTaxClass(taxClassStr);
    }        
    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    public void setYear(String year) {
        this.year = Integer.parseInt(year);
    }
    /**
     * @return the type
     */
    public TaxType getTaxType() {
        return taxType;
    }

    public String getTaxTypeStr() {
        return taxType.toString();
    }
    /**
     * @param type the type to set
     */
    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    public void setTaxType(String typeStr) {
        this.taxType = TaxType.valueOf(typeStr);
    }
    
    public TaxClass getTaxClass() {
        return taxClass;
    }
    
    public String getTaxClassStr() {
        return taxClass.toString();
    }
    
    public void setTaxClass(TaxClass taxClass) {
        this.taxClass = taxClass;
    }
    
    public void setTaxClass(String taxClassStr) {
        this.taxClass = TaxClass.valueOf(taxClassStr);
    }
    
    /**
     * @return the min
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(BigDecimal min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(BigDecimal max) {
        this.max = max;
    }

    /**
     * @return the rate
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
        

           
}
